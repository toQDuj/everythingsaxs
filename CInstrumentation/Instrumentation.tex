% !TEX encoding = UTF-8 Unicode
\chapter{Instrumentation}
\section{Historical overview}

From the inception of SAXS around the 1930's, significant effort was expended on improving the data obtained from the instruments as it became clear to the early researchers that what you get out of it depends on what you put into it (i.e. that the quality of the results were linearly dependent on the quality of the data collected). 
A good overview of the early efforts is given by \citet{Bolduan-1949}. In particular, advances in collimation led to the widespread use of three collimators to reduce background scattering \cite{Bolduan-1949,Worthington-1956}, focusing and monochromatisation crystals (and even practical point-focusing monochromators \cite{Fankuchen-1937,Fankuchen-1939,Shenfil-1952,Furnas-1957}), high-intensity X-ray sources and total reflective mirrors. 
These early developments have led to near-universal adoption of all of these elements in subsequent instruments to improve the flux and signal-to-noise ratio.

X-ray sources in particular have increased drastically in brightness, leading to a similar increase in photon flux at the sample position for many small-angle scattering instruments. 
Where initially photon fluxes from laboratory sources were on the order of $10^3$ to $10^4$ photons per second (at the SAXS instrument sample position, estimated using \citet{Fankuchen-1939}). 
This has increased to the current flux from micro source tubes and rotating anode generators of about $10^7$ photons per second, useful for most common x-ray scattering experiments. 
For monitoring of dynamic processes, position-resolved, or SAXS tomography experiments where higher flux is required, synchrotron-based instruments can deliver around $10^{11}$ to $10^{13}$ photons per second to the sample environment. 
On specialised beamlines such as BL19LXU at the SPring-8 synchrotron, fluxes of $10^{14}$ photons per second can be obtained. 
X-ray lasers such as SACLA in Japan, the European XFEL and the LCLS in the US provide very intense \emph{pulses} of X-rays, but the total flux is typically only about $10^{11}$ photons per second.
		
The thus obtained increase in flux and reduction of parasitic scattering was further exploited by the advent of new detection systems.
The first SAXS instruments employed step-scanning geiger counters \cite{Jellinek-1946} or photographic film (with a notable instrument even using 3 photographic films simultaneously \cite{Hermans-1959} so that sufficient information could be collected to measure in absolute units \cite{Heikens-1959}), which were rather laborious and time-consuming detection solutions.
The photographic films in particular had a very nonlinear response to the incident intensity, necessitating complex corrections \cite{Chantler-1993}.
The advent of image plates \cite{Cookson-1998} and 2D gas-filled wire detectors \cite{Gabriel-1982} mostly replaced the prior solutions, though image plates have a low time resolution (given the need to read and erase them), and the 2D gas-filled wire detectors suffer from a low spatial resolution due to a considerable point-spread function \cite{Le-Flanchec-1996}.
Charge coupled device (CCD) detectors enjoy a modicum of success, though they suffer from reduced sensitivity alongside a slew of other issues \cite{Barna-1999}. 
A costly but overall relatively problem-free detector came about with the development of the direct-detection photon counting detector systems such as the linear position sensitive MYTHEN detector \cite{Schmitt-2003}, the 2D PILATUS detector \cite{Eikenberry-2003}, its upcoming successor, the EIGER detector \cite{Johnson-2012}, and the Medipix and PIXcel detectors \cite{Campbell-2011}. 
The required corrections for these detectors will be discussed in \S \ref{sc:detcor}.



\section{Instrumentation classes}

While in the past many instruments were designed and built in-house, nowadays many good instruments can be obtained from a large variety of instrument manufacturers. 
Given the current ease of obtaining money for a complete instrument rather than instrument development, and the drastic reduction in time required between planning and operation, the extra cost involved may in many cases be offset by the benefits. 
These instruments come in a variety of flavours and colours, but can essentially be divided into three main classes: 
1) pinhole-collimated instruments, 
2) slit-collimated instruments, and 
3) Bonse-Hart instruments relying on multi-bounce crystals as angle selectors \footnote{A good review of instrumentation is also given by \citet{Chu-2001}. 
Furthermore, tools for instrument design evaluation have recently become available \cite{Knudsen-2013}}.
		
		\subsection{Pinhole-collimated instruments}
			\begin{figure}
				\centering
		   		\includegraphics[angle=0, width=0.95\textwidth]{CInstrumentation/images/scattering_to_small_angles_setup} 
		   		\caption{The required components for a small-angle scattering experiment. \label{fg:sass}}
			\end{figure}

The first of these three, pinhole-collimated instruments (schematically shown in Figure \ref{fg:sass}) have become very popular due to their flexibility in terms of samples and easy availability of data reduction and analysis procedures. 
While initially eschewed for slit-collimated instruments due to the drastically higher primary beam intensity of the latter, improvements in point-source X-ray generators as well as 2D focusing optics have reduced the weight of this argument somewhat. 
These type of instruments also dominate the small-angle scattering field at synchrotrons as well as neutron sources due to their aforementioned flexibility.
			
%discuss source types? Wiesmann-2009 for I\muS
These instruments typically consist of a point-based X-ray source followed by X-ray optics. 
These optics are either used to parallelise the photons emanating from the source, or focus the X-rays to a spot on the detector or sample. 
After the X-ray optics, the beam is then further collimated using either three or more collimators made from round pinholes or sets of slit blades, separated by tens of centimetres to several meter (a particular effect of the collimation on beam properties is given in \S \ref{sc:coh}). 
While the third collimator was required to remove slit or pinhole scattering from the second collimator \cite{Bolduan-1949,Wignall-1990,Pedersen-2004}, the recent development of single-crystalline ``scatterless'' slits remove the need for the third collimator \cite{Gehrke-1995,Li-2008}. %apparently, Hosemann already used a third pinhole in 1939 according to Wignall-1990
			
There are two main instrument variants in circulation as to what happens after the collimation section. 
One type of instrument ends the in-vacuum collimation section with an x-ray transparent window, allowing for an in-air sample placement and environment before entering another x-ray transparent window delimiting the vacuum section to the detector (this sample-to-detector vacuum section is also known as the ``flight-tube''). 
As this introduction of two x-ray transparent windows and an air path generates a non-negligible amount of small-angle scattering background itself, it does not lend itself well to samples with low scattering power \cite{Dubuisson-1997}. 
The second instrument variant, therefore, consists of a vacuum chamber (and often a vacuum valve which can be closed to maintain the vacuum in the flight-tube during the sample change procedure), and thus allows an uninterrupted flightpath from collimation through the sample into the flight tube. 
While this generates the least unwanted scattering, it does add restrictions to the sample and sample environments that can be put in place \cite{Pedersen-2004}.
			
At the end of the flight tube sits the in-vacuum beamstop, whose purpose is to prevent the transmitted beam from damaging the detector or causing unwanted parasitic scattering, and can be one of three types. 
This beamstop can be a normal beamstop, which blocks all of the transmitted beam. It is useful in many cases, however, to have an estimate for the amount of radiation flux present in the transmitted beam. 
For this purpose, the beamstop can be replaced or augmented with a small PIN diode, which measures the flux directly (albeit on arbitrary scale), or the beamstop can be made ``semi-transparent'', meaning that the beamstop is adapted to pass through a heavily attenuated amount of radiation which subsequently falls onto the detector. 
The presence of either of the two latter options can be used to benefit the accuracy of the data reduction step, leading to more accurate data and therefore more accurate results. 
			
Finally, the flight tube exits in a window followed (almost) immediately by the detector. 
For detectors with a large detecting area, this exit window (and the flight-tube exit section) must be engineered to be strong and large, sometimes leading to visible parasitic scattering from the window material. 
It is therefore recommended to keep the detector small, allowing for a small and modular flight tube with very little exit window issues. 
Alternatively, for very modern systems, some detectors can work in-vacuum as well which removes this last (small) source of parasitic scattering and allows for step-less translation of the detector and beamstop within this vacuum, drastically increasing the flexibility in angular measurement range. 
			
One alternative to this type of instrument was the "Huxley-Holmes" camera which contained two separate optical components for monochromatization and focusing, to achieve a very low background \cite{Zemb-2003}.
While this instrument is performing well, the authors currently recommend going for a more common configuration instead consisting of focusing optics followed by scatterless slits \cite{Zemb-2013-pc}.
		
		\subsection{Slit collimated instruments}\label{sc:slit}
		
A second type of instrument exists which is much more compact than the pinhole-collimated systems, is less expensive and illuminates a larger amount of sample to collect more scattering. 
This type of instrument is often referred to as a ``Kratky'' or ``block-collimated'' camera, perhaps best explained in \citet{Kratky-1963} and \citet{Glatter-1982}. 
This camera is commonly built on an X-ray source emitting a beam with a line-shaped cross-section, and collimates the x-ray beam using rectangular blocks of metal\footnote{A subsequent interesting improvement by \citet{Schnabel-1972} using glass blocks in the collimation system did not catch on, whereas beam monochromatisation and/or focusing has been a quite widely implemented improvement \cite{Fritz-2006}}. 
While this instrument is sometimes referred to as an ultra-small-angle scattering instrument, it is typically used as a normal small-angle scattering instrument.
			
The line-shaped cross-section of the X-ray beam does bring with it a major drawback, in that the collected scattering pattern is substantially different from the pattern one would obtain from a pinhole-collimated instrument, and therefore needs a modified data correction procedure. 
Effectively, the scattering pattern is distorted or blurred due to a superposition of intensity contributions from various scattering points along the line-shaped beam.
While the collected ``slit-smeared'' scattering patterns can be subjected to a numerical correction to compensate for this smearing effect, such de-smearing processes in the best case merely amplify the noise in the system and in the worst case introduces artefacts which could be mistaken for real features \cite{Vad-2011}. 
This de-smearing procedure will be discussed in more detail in \S \ref{sc:SM}. 
Furthermore, analysis of samples containing an anisotropic structure becomes more tedious, leaving the instrument most suited to isotropically scattering samples.
			
There are a number of instruments preceding the block-collimated camera, which nonetheless employed a line-shaped X-ray beam collimated with a series of slits instead \cite{Ritland-1950,Worthington-1956,Hermans-1959}. 
While these formed the basis of the first SAXS instruments, and are by definition slit-collimated instruments, they are no longer in widespread use.
			
		\subsection{Bonse-Hart instruments}\label{sc:bh}
		
A third type of instrument is one particularly suitable for ultra small-angle scattering purposes (for the analysis of larger structures typically from several nanometer to several microns), and is known as the ``Bonse-Hart'' camera \cite{Bonse-1966}. 
These instruments utilise the high angular selectivity of crystalline reflections to single out a very narrow band of scattering angles for observation, i.e. using the crystals as angle selectors both for collimation- as well as analysis purposes. 
While the idea of using crystalline reflections was not new \cite{Fankuchen-1939,Ritland-1950}, the advantage of the implementation by \citet{Bonse-1966} was the use of multiple reflections to improve the off-angle signal rejection in a straightforward manner \cite{Chu-1992}.
			%ease of use and improved angular selectivity of implementing channel-cut crystals rather than separate or single-bounce crystal elements. 
			
The incident beam is collimated to a highly parallel beam through multiple crystalline reflections rejecting all but the angles in reflection condition. 
The sample is placed into this parallel beam effecting small-angle scattering as the beam passes through the sample. 
 second crystal (a.k.a. ``analyser crystal'') is then used to pick out a single angular band of the scattered radiation. 
Through rotation of the analyser crystal, the scattered intensity at various angles can be evaluated with an extremely high angular precision c.q. resolution. 
A few standalone instruments have been generally constructed on synchrotrons \cite{Chu-1992,Diat-1995,Ilavsky-2009a,Ilavsky-2013}, and several more have been built as complementary instruments around laboratory x-ray sources (tube- as well as rotating anode sources) \cite{Bonse-1966,Gravatt-1969,Lambard-1991,Chu-1992}.
			
			%added in review:
The instrument angular resolution is defined mostly by the rocking curve of the crystalline reflection, also known as the ``Darwin width'', which is the angular bandpass window of the crystalline reflection \cite{Barker-2005,Ilavsky-2013}. 
While a large variety of crystalline materials can be used in the instrument, the channel-cut crystals are usually made from either silicon or germanium due to their high degree of crystalline perfection over large sizes \cite{Barker-2005}. 
These crystals have Darwin widths (FWHM) for the common (111) and (220) X-ray reflections of about 0.0002 degrees, thus defining the $q$-resolution for most such instruments to be about 0.001 nm$^-1$ (where the multiple reflections do not change the darwin width, but improve the off-reflection rejection) \cite{Chu-1992,Sztucki-2007a,Diat-1995,Gravatt-1969,Lambard-1991}. 
When a higher resolution is required, for example to measure larger structures, the combination of high-energy and higher-order crystalline reflections can lead to a ten-fold increase in resolution \cite{Ilavsky-2013}. 
Neutrons can also be used instead of X-rays, as the darwin width for a neutron reflection is about 0.105 times that of the X-ray counterpart, which can therefore lead to a similar increase in resolution \cite{Barker-2005}.
			
As the channel-cut crystals only collimate in one direction, these instruments suffer from a similar slit-smearing effect as the Kratky-type instruments discussed in \S \ref{sc:slit}. Desmearing of the data is therefore required, unless effort and intensity is expended to collimate the beam in the perpendicular direction as well \cite{Lake-1967,Gravatt-1969}. 
An additional drawback to these instruments is the requirement for a step-scanning evaluation of the scattering curve \footnote{though there are efforts in neutron scattering to overcome this limitation \cite{Niel-1991}}, which increases measurement times considerably. 
Due to the fast intensity falloff at higher angles, and the extremely narrow angular acceptance window of the analyser crystal, this instrument performs best at ultra-small angles but has much reduced efficiency at larger angles. 
These properties render this type of instrument a useful \emph{addition} to existing SAXS instrumentation, but is less frequently encountered as a standalone instrument. 
Given the particulars of the data, Bonse Hart data correction may require additional consideration (e.g. for the determination of the sample transmission factor and the effects of the rocking curve on the data) \cite{Chu-1992,Sztucki-2007a,Zhang-2010}. %added
		
While the difference between a Kratky camera and a Bonse-Hart camera initially seemed to be in favour of the Kratky camera \cite{Kratky-1970}, it gradually became clear that both instruments have their place in the lab. 
For small-angle scattering measurements on weakly scattering systems at common small angles (i.e. $0.1\leq q$ (nm$^{-1}$) $\leq 3$), a Kratky camera performs very well, while for measurements to very small angles (i.e. below $q$ (nm$^{-1}$) $\approx 0.1$) the Bonse-Hart approach would be the preferred instrument \cite{Deutsch-1980,Chu-1992}. 
			
		\subsection{A note on collimation and coherence}\label{sc:coh}
					
In typical scattering measurements, only a fraction of the volume is irradiated with \emph{coherent} radiation (i.e. with in-phase electromagnetic fields), therefore only that fraction of the irradiated sample volume contributes to the scattered intensity \cite{Veen-2004}. 
In other words, the irradiated sample volume typically contains a multitude of so-called ``coherence volumes'', each of which contributes to the scattering pattern. 
As there is no inter-volume coherence, it is the sum of the scattering intensities (as opposed to the sum of the amplitudes) from each of these volumes that is detected \cite{Livet-2007}.

These coherence volumes are defined by two components, the longitudinal component (parallel to the beam direction) and the transversal component (perpendicular to the beam direction, c.f. Figure \ref{fg:intro_coherence}). 
The longitudinal component is dependent on the degree of monochromaticity of the radiation, and is large for monochromatic radiation and quite small for polychromatic beams \cite{Livet-2007}.
 The transversal dimension $\zeta_t$ of the coherence volume is defined through the collimation, in particular through the dimensions of the beam-defining collimator and its distance to the sample, and can be estimated as \cite{Veen-2004}:
			\begin{equation}\label{eq:coh}
				\zeta_t=\frac{\lambda l}{w}
			\end{equation}
where $\lambda$ is the wavelength of the radiation, $l$ the distance between the beam-defining collimator and the sample, and $w$ the size of the collimator opening ($\zeta_t$ can be calculated for each direction for collimators with nonuniform openings). 
			
The estimation of the transversal coherence length is an important check for experiments. Scattering objects with dimensions close to or larger than the transversal coherence length may not contribute significantly to the small-angle scattering as the coherence volume will be within a uniform region of material (an effect seen amongst others by \citet{Rosalie-2014}). 
This effect can be exploited to investigate the actual transversal coherence length in an instrument as shown by \citet{Gibaud-1996}. 
For a more detailed treatment of coherence (i.e. when it is approaching significance or what happens when a \emph{single} coherence volume encompasses the sample), the reader is referred to the aforementioned literature. 

			\begin{figure}
			   \centering
			   \includegraphics[angle=0, width=0.65\textwidth]{CInstrumentation/images/intro_coherence_small} 
			   \caption{Coherence volume after a slit. The larger the slit, the smaller the transversal coherence length. \label{fg:intro_coherence}}
			\end{figure}



\section{Sources}
\section{Optics}
\section{Collimation}
\section{Detectors}
\section{In-house designs}
\section{Commercially available designs}



\section{Performance comparisons}
\subsection{Comparisons of Doom! Line vs. Pinhole}
\emph{SOURCE: \url{www.lookingatnothing.com/index.php/archives/1309}}
\subsubsection{Premise}
The typical argument in favor of the line-collimated "Kratky"-type instruments is that its X-ray flux is very high and that therefore you need only short measurement times. However, its data needs to be corrected through a "desmearing"-procedure, amplifying uncertainties and noise in the process. Does this approach then really give you better data? Let's find out! 

\subsubsection{Introduction}
The instruments available for this comparison were an Anton Paar SAXSess system (\url{http://www.anton-paar.com/}) and a Bruker Nanostar (\url{http://www.bruker.com/products/x-ray-diffraction-and-elemental-analysis/x-ray-diffraction/nanostar/overview.html}). These are common instruments, but have one major difference: the SAXSess is a line-collimated "Kratky" instrument, the Nanostar is a pinhole-collimated system. Note that I am not interested here in comparing the manufacturers at all, but rather the underlying concepts.

The sample chosen for this is one of the glassy carbon standards produced by Jan Ilavsky, which comes complete with its own calibrated datacurve to compare against. The sample was measured for 10 minutes on both instruments (which was considered long enough by the Anton Paar representative who happened to visit).

Caveats: both instruments suffered from a drawback complicating direct comparisons; the SAXSess is on a tube source whereas the Nanostar is on a rotating anode generator. On the other hand, the SAXSess uses a nice sensitive and accurate Dectris Mythen detector, whereas the Nanostar is using an inefficient (at the used energy) gas-filled wire detector. So it is not a perfect comparison, but we make do with what we have.

\subsubsection{Results and Discussion}
Andreas Th\"u nemann kindly measured the sample using the Anton Paar instrument and corrected the data according to their recommended procedures. Before desmearing, the data looks to be of beautiful quality: low noise and small error bars (black curve, Figure \ref{fg:kratky_data}). After desmearing, however, the uncertainties are much larger, and there is a visible structure in the data which is not expected for glassy carbon samples.

\begin{figure}
   \centering
   \includegraphics[angle=0, width=0.65\textwidth]{CInstrumentation/images/Kratky_data} 
   \caption{Raw and desmeared curves of glassy carbon measured for 10 minutes on a SAXSess instrument. \label{fg:kratky_data}}
\end{figure}

The Nanostar data (Figure \ref{fg:pinhole_data}) does not need desmearing (though it can be considered), but it has been heavily corrected using the in-house developed imp2 correction software. The data shown shows the smaller measurement range of the instrument (mostly because of the relatively poor detector performance), and shows a generally consistent scattering profile without outstanding features beyond the uncertainties.

\begin{figure}
   \centering
   \includegraphics[angle=0, width=0.65\textwidth]{CInstrumentation/images/pinhole_data} 
   \caption{Corrected curves of glassy carbon measured for 10 minutes on a Nanostar. \label{fg:pinhole_data}}
\end{figure}

So, how can we compare these datasets accurately to the calibrated curve supplied by Jan Ilavsky? First of all, the SAXSess data was binned to 100 logarithmically-spaced datapoints, in line with the bins from the Nanostar data correction procedure. The curves were then matched to the calibration datacurve using an error-weighted least-squares fitting procedure also allowing for a flat background contribution. The relative difference was then calculated for comparison.

\begin{figure}
   \centering
   \includegraphics[angle=0, width=0.65\textwidth]{CInstrumentation/images/Complete_comparison} 
   \caption{Comparison between the SAXSess, Nanostar and calibration scattering curves for Glassy carbon. Bottom: relative deviation from the calibrated data. \label{fg:complete_comparison}}
\end{figure}

The results are quite informative (Figure \ref{fg:complete_comparison}). Mostly, the oscillations of the (blue) SAXSess scattering are more pronounced when directly compared with the (red) calibration data. The (green) Nanostar data, however, does deviate noticeably from the calibration data at low Q, which convinces me to re-check my data corrections.

The relative deviation plot, though, shows the real answer to the posed question. The required desmearing procedure applied to the SAXSess data negates any benefit there may have been of the increased photon flux. The data subsequently suffers from deviations in intensity in excess of +/- 10\%. The Nanostar data shows better consistency with the calibrated data of +/- 5\%, though that is not as good as I had hoped it would be.

In conclusion, then, please take care to measure on line-collimated instruments for a longer time. Try to compare the data of calibration samples to estimate the data uncertainty and be careful to optimize the desmearing procedure to minimize the resulting error magnification. 

\subsubsection{Relevant comment from Sylvain Pr\'e vost}
% TODO: import references
\begin{quotation}
To me, the biggest issue by far with Kratky cameras is the danger of entirely overlooking features. In particular in Soft Matter, one often measures scatterers near Zero Average Contrast conditions (e.g. most micelles, with an excess electronic density in the shell), leading to a sharp minimum (inversion of the sign of the form factor amplitude at mid q). This sharp minimum can be completely missed with a Kratky camera, the de-smearing procedure providing a spectrum with a plateau at mid q that can be understood as due to repulsive interaction. Such an example exists in the literature with DPC micelles, and it is instructive to compare the synchrotron data from APS (Lipfert et al. \url{http://dx.doi.org/10.1110/ps.051874706}) to SAXSess data (Glatter et al. \url{http://dx.doi.org/10.1021/jp9114089}). In log scale, the difference is striking.
\end{quotation}

\section{Odd instruments}