—INTRODUCTION—

This book about SAXS is a citeable living document. When citing, please refer to a particular commit signature, indicating the exact state of the book at a particular point in time (how to get that will be indicated here in the future).

—Building the book—

The book can be turned into a PDF by compiling it with LaTeX. At the moment, my weapon of choice for this is an Apple iMac running OSX 10.9.5. I installed the monstrously big MacTex collection of packages on it, and a new version of TeXShop as editor (and front-end to the compiler). It uses a script using Tex and DVI to convert the document to a PDF file. 

This will be amended to a more OS agnostic compilation script in the near future, including the hyperref package (probably) to enable internal and external links. 

A current copy can be made available on request by sending an e-mail to the main author/editor. 

—Contributing—
This work is intended to become a communal effort. The Git version control system is designed to do exactly this. All contributing authors are to be listed (hopefully with statistics and specifics, eventually) in the book.

Minor or one-off contributions can also be submitted to the author by e-mail. 

All content is licensed under the latest international Creative Commons attribution license (CC-BY 4.0 at the moment), although sections may be licensed under an older version where indicated. All images are to be licensed under a CC license as well. This license allows free re-use of all or part of the content provided proper attribution is given.

All text should be saved as Unicode UTF-8 encoded files. 