% !TEX encoding = UTF-8 Unicode
\chapter{Analysing SAXS patterns}
\section{Chapter organisation}
How do we get from scattering pattern to morphological information in a straightforward way?

For me, this typically takes about three steps: 
\begin{enumerate}
\item{} Checking that there are no obvious problems with the data corrections
\item{} the initial look 
\item{} some simple fits and 
\item{} the final fit. 
\end{enumerate}
There is a possible fifth step: advanced fits, but this is not a necessity in some cases. 



\section{Before looking: Checking the corrections}



\section{The first look: Curve shape}
\emph{SOURCE: \url{http://www.lookingatnothing.com/index.php/archives/846}}

So imagine you have collected a scattering pattern and subjected it to the applicable corrections to get a background-subtracted, corrected scattering pattern. 
This sets the stage for step 1. 
In step 1, we check if there is any suitable information in the scattering pattern at all, and we collect information from other techniques.

What I would do is to plot the data on a double-logarithmic plot, in intensity versus scattering vector: $\log(I)$ versus $\log(q)$. 
Bonus points if you can plot error bars on the intensity. 
You can then try to identify different features in your scattering pattern. 
At least you should be able to see an asymptotic behaviour towards a constant (flat) background. 
Furthermore, you can likely identify a straight region of a constant slope which is called the Porod region. 
A simple example exhibiting many features is shown in Figure \ref{fg:step1regions}. 
We will discuss the features starting from those at highest q-values.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/regions}
    \caption{Simulated scattering pattern of polydisperse cylinders (with a background), where typical SAXS regions have been identified.}
    \label{fg:step1regions}
\end{figure}

The constant background contribution can arise from a plethora of causes. 
It can come from very small-scale inhomogeneities in one or both of the phases in your sample, or remains from insufficient background subtraction or insufficient correction for detector darkcurrent levels \cite{Ruland-1971,Koberstein-1980}. 
There is a modicum of information in the background level, but usually it is partially or wholly artificial.

The Porod region is another region of limited use. 
The scattering intensity in this region should be proportional to a $q^{-4}$ slope, but can also assume values lower or higher than that \cite{Ruland-1971,Koberstein-1980,Sobry-1991}. 
The $q^{-4}$ scattering arises from large scatterers beyond their characteristic scattering region. 
In other words, it indicates that there are scattering objects, but it does not tell you what size they are. 
We need to look at lower q-values to get that information.

Porod slopes smaller than $q^{-4}$ could indicate the presence of a fractal-like structure in your sample: a self-similar structure over a certain size range. 
This could be a rough surface, but can also arise in very polydisperse mixtures \cite{Teixeira-1988,Beaucage-1996}.

At q-values lower than the Porod slope, there can be a region which contains the most information in the scattering pattern which I shall call the Debye region (after the early description of this region by Debye, B\"uche, Anderson and Brumberger \cite{Debye-1949,Debye-1957}). 
This region typically has a slope between $q^{-1}$ and $q^{-3}$, and ---when analysed--- provides the size distribution of the structure.

Some say that a particular slope identifies the shape of the scattering entities (discs, cylinders or spheres), but this is not completely unambiguous. 
As will be shown later, the scattering pattern from polydisperse cylinders can be perfectly described using spheres as well (though perhaps the reverse does not hold, I did not test that yet…). 
All that can be said, then, is that this region is the one carrying the information.

Before that Debye region lies the Guinier region: a flat region independent of q. 
This contains information on the volume-square weighted size of the scatterers, and also can be used to find the total volume fraction of scatterers in the sample. 
As it typically lies at very low q values (and so very close to the beamstop), it is also most easily affected by parasitic scattering and other unrelated scattering effects. 
In short: This region has some information, but is prone to fouling.

Figure \ref{fg:step1regions} shows the best case scenario for data: that in which all regions are clearly visible and distinct from one another. 
I will provide a few more examples to indicate some alternatives you might find in your scattering patterns.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/Disc_gauss_s10_m30_bgnd1e1_q4slope_sasfit}
    \caption{Simulated scattering of polydisperse discs in the presence of large scatterers and a flat background contribution.}
    \label{fg:step1ex1}
\end{figure}

Figure \ref{fg:step1ex1} shows another situation where there is a certain amount and distribution of discs (the hump in the middle), and a background contribution. 
I also simulated the presence of some larger structures (i.e. aggregates or simply large scattering objects), of which only the $q^{-4}$ slope is visible. 
The presence of this slope makes it more difficult to distinguish the individual regions, but the hump is the area of interest. It is that area which contains the information on the discs.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/Sphere_gauss_s10_m30_bgnd1e2_q4slope_struct_0p22_sasfit}
    \caption{Simulated scattering of densely packed polydisperse spheres in the presence of large scatterers and a flat background contribution.}
    \label{fg:step1ex2}
\end{figure}

Figure \ref{fg:step1ex2} shows an interesting case of scatterers (spheres in this case) which are quite densely packed. 
This dense packing causes inter particle interference which shows up as a depression in the scattering pattern. 
It may not necessarily be as clear as this, and its visibility can reduce due to a plethora of effects (polydispersity, background, etc). 
Structure factors can be a pain in the ass to fit. 
Diluting the sample to remove this effect is recommended if possible.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/Sphere_gauss_s20_m30_bgndm1e3_q4slope_0p22_sasfit}
    \caption{An extreme case of oversubtraction of a background signal. The marked drop at high q is a clear indication.}
    \label{fg:step1ex3}
\end{figure}

A common problem in data correction is that of oversubtraction of the background measurement (because people tend not to correct for transmission). 
Fortunately, it is easy to recognise this mistake, as shown in Figure \ref{fg:step1ex3}: there is a marked drop to negative values of the scattering signal at higher q-values.

The simulations shown here are made using SASfit by Joachim Kohlbrecher and Ingo Bressler (\url{http://sourceforge.net/projects/sasfit/})
I highly recommend downloading that software, to learn how to use it and to simulate a few scattering patterns to get a feel for what small-angle scattering can look like. 
Another good package is the Irena package by Jan Ilavsky for Igor Pro (\url{http://usaxs.xray.aps.anl.gov/staff/ilavsky/irena.html}).

\subsection{Summary}
At the end of this step, you should have:

\begin{itemize}
\item{}Complementary information on your structure such as an electron micrograph, X-ray absorption data, crystallographic information, or the production process to name but a few. While I did not touch on this much before, do not proceed without such information as you need to be able to substantiate the structural assumptions that go into the fitting procedure. If you just start fitting, you will end up with a model that matches the scattering behaviour, but has no basis in the actual nanostructure. From the complementary information you should be able to deduce what kind of scattering entities you have in your sample.
\item{}An idea of the background level. If you do not see a constant background level appear, double-check that the background subtraction has not been overdone. An over-subtraction of the background often occurs when the differences in transmission factor (in the sample and background measurements) have not been taken into account
\item{}An idea about whether or not a constant slope should be taken into account (originating, for example, from inter particle scattering and/or large-scale structures.
\item{}Information on whether or not you should consider a structure factor. This may not always be apparent from the scattering pattern.
\item{}Whether or not there is anything interesting at all in the scattering pattern. If not, consider measuring in a different size range or re-evaluating your sample.
\end{itemize}



\section{The second look: Guiding methods, simple fits}
\emph{SOURCE: \url{http://www.lookingatnothing.com/index.php/archives/925}}

% TODO: move 2 paragraphs to previous section?
What was not discussed in the previous section is that the scattering pattern may have a peak as well, usually appearing at the larger q-values. 
This can be a diffraction peak from a long-period structure. 
If there is an upswing in your data at the higher q-values, you might be seeing only the tail of such a peak, or you might have performed insufficient subtraction of the background scattering signal.

An idea about the approximate size of the scatterers can also be obtained in some cases. 
If you see a "hump" in your log-log plot, their radius can be approximated by determining the q-value of the peak, and simply calculating $\pi/q_{peak}$. 
This should give you a rough idea about what size-range your structures are. 
As indicated by the tone of the preceding sentences: don't just publish this figure!
% end move

For this fitting explanation, I will be using SASfit from J. Kohlbrecher and I. Bressler, but other programs may be usable as well. 
I have adapted a real dataset for this explanation, from precipitates in a metallic sample. 
This dataset can be downloaded from here.

What we are trying to obtain in this step of the fitting procedure is an idea what the final model should contain. 
If we have more than one sample in the series (always a good idea, a blank and some intermediate samples to show evolution), we have to test this model on a few samples of the same series. 
A single model that is capable of describing the scattering from most samples is much better than a tuned model per sample as it will be much easier to show interrelationships.

The first goal is to describe the background contributions in the scattering pattern. 
A quick glance at the data shows a sloped background feature. 
While there is no clear indication of a flat background, we usually include it anyway. 
With SASfit, you might need a bit of perseverance and patience, but you'll get there (in particular, "apply" the proposed parameters first before starting the fit, and only start the fit if the initial configuration intersects the data). 
The result of this can be seen in Figure \ref{fg:step2p1}.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/test_step1}
    \caption{Fit of the sloped background and constant background to the data.}
    \label{fg:step2p1}
\end{figure}

This fit doesn't look like much, but it's a start. 
We can now add a contribution of scattering objects to describe the rest. 
If you know you have spherical scatterers in your sample, add spheres. 
Choosing to start simple with spheres conforming to a Gaussian size distribution already fits the scattering pattern quite well (Fig. \ref{fg:step2p2}).

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/test_step2}
    \caption{Previous fit to which a contribution of spheres has been added. sphere size distribution modeled assuming a Gaussian.}
    \label{fg:step2p2}
\end{figure}

The electron micrograph shows that we have cylinders, not spheres (trust me on this). 
So selecting long cylinders returns a fit which is very similar (also in terms of reduced chi-square, which is slightly larger than 8 in both cases). 
You cannot tell (comparing Fig. \ref{fg:step2p3} with Fig. \ref{fg:step2p2}) which is better from the fit itself, which is exactly why the supplementary information is so important: the fit will fit, but the numbers will be wrong!

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/test_step2cyl}
    \caption{Previous fit, but instead of spheres, we are fitting using long cylinders.}
    \label{fg:step2p3}
\end{figure}

Given the limitations of the classical analysis methods, this is where I would stop in this fit. 
One should try out a few more distribution shapes (log-normal, Lorentzian) to get a slightly better fit, but adding contributions would be unwise. 
Adding contributions would destabilise the fit quite quickly, and might make the model justification exponentially harder. 
Also, do not "hunt" for a fitting model that describes the data best, and then try to argue why that model should be correct. 
Your model choice should be firmly grounded in other evidence.

One other thing you can do to try to fit your data, is to apply Greg Beaucage's unified fit function. 
This function is a combination of two scattering simplifications: a Guinier-like behaviour and a Porod-like relationship. 
While this function is based on a rather severe set of assumptions (detailed in his publications), it has booked several successes in describing multi-scale scattering effects. 
This function might be able to provide some insight into the size range of the structures under investigation.

While this is only a straightforward example, there are an enormous amount of alternative fitting functions available for the strangest scatterers. 
Chances are that one has been developed for your system as well over the past 80 years of scattering, so please search for the most appropriate function and do not automatically head for the Porod, Guinier, Debye-Anderson-Brumberger or other heavily approximated functions. 
And always verify that the answer you get from the fitting is realistic.

\subsection{Summary}
At the end of this step, you should have:

\begin{itemize}
% TODO: fill in
\item{}something [WIP].
\end{itemize}


\subsection{Fitting parameters}
fitting data (asterisk indicates inclusion in optimisation function):

Long cylinder model (L=70, eta=0.1) w/gaussian distribution (N=1.6*, s=2.64*, X0=3.42*)  and added background contribution (c0=5.35*, c1=0, c4=16.6*, alpha=4). 
Reduced chi-squared: ~8.6

Sphere model (eta=0.1) w/gaussian distribution (N=8.15*, s=3.26*, X0=3.38*) and added background contribution (c0=5.28*, c1=0, c4=29.6*, alpha=4). 
Reduced chi-squared: ~8.5]



\section{The third look: Least-squares analysis with classical models}
\section{Look again: Advanced methods}


\section{Spheres, rods, discs, which can fit what?}
\emph{SOURCE: \url{http://www.lookingatnothing.com/index.php/archives/1397}}

\subsection{Problem setting}
A recent paper by Dr. Yojiro Oba briefly discusses which shape assumption can be appropriate to fit a scattering pattern of polydisperse systems \cite{Oba-2014}. 
In the paper, the shape assumption is spherical (based on TEM evidence), and a further remark goes as follows:

\begin{quote}
Since no $q^{−a}$ (a < 4) behaviour is observed, the possibility of anisotropic shapes such as a rod, disc, and ellipsoid of revolution is denied.
\end{quote}

That is an elegant way of putting it (note that it is an unidirectional exclusion and does not work the other way), and it sounds about right. 
So let's test this with some simulations. 
These, of course, cannot prove that the statement is true, but can only disprove the statement.

\subsection{Methods}
For the simulations, we use the familiar SASfit software (version 0.94.5), whose stability has much improved in recent years. 
For fitting, we use the McSAS Monte Carlo method. 
The uncertainty estimates have been calculated using Excel in between the simulation and fitting procedures.

For this test it would be interesting to investigate the three commonly used shapes: spheres, thin rods and flat discs. 
I want to find out whether any of these shapes can be reasonably described with any of the other models, e.g. if we can fit a scattering pattern from flat discs with a sphere model, and a scattering pattern from spheres with a thin rod model, etc..

For the sake of clarity, we define a rod here as a long, cylindrical object, with the radius (0.5 nm) well below the size limit imposed by the Q-range (3 nm), and the length distribution within range. 
We define a disc as a flat cylindrical object, with the radius within range and setting the length (0.5 nm) well below the Q-range imposed size limit (3 nm). 
The simulation settings are specified for spheres, discs, and rods in \S \ref{ssc:srdSim} below ([s1], [s2], and [s3], respectively). 
The scattering patterns are shown in Figure \ref{fg:srdCurves}.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/curves}
    \caption{Scattering for a variety of shapes.}
    \label{fg:srdCurves}
\end{figure}

\subsection{Result and Discussion}
The results (Figure \ref{fg:srdTable}) are in agreement with the statement above. 
We can fit any simulated scattering pattern from rods, spheres or discs using spheres, but not necessarily the other way around. 
So the statement in the paper appears to hold up for these shapes. 
This is also evident from the shape of the scattering patterns (Figure \ref{fg:srdCurves}) where the less sloped curves would be challenging to match to the steeper-sloped curves.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/resulttable}
    \caption{Table showing which model can be used to fit scattering patterns from a variety of polydisperse shapes.}
    \label{fg:srdTable}
\end{figure}

That should have been the end of that story, but I decided to go a little further (a simulation too far, perhaps).

Things might get more complicated with other models (e.g. long cylinders (fitting radius), wide plates (fitting thickness). 
As a test, I tried simulating long cylindrical objects (see [s4] below), whose length exceeds the Q-range limit, but the radius range falls squarely in it.

From our diagram, we should be able to fit pretty much anything to polydisperse spheres. 
This model gets there, but only with the addition of a few extra contributions (500 instead of 200).

Flat platelike objects ([s5]), on the other hand, can *almost* be fit with spheres, but not quite completely. 
Whether this is a failure of the simulation, the fitting method or the assumptions, I cannot yet tell. 
It looks like it should be possible, but the vast span of uncertainty estimates may be reducing the optimisation speed of the MC algorithm.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{CAnalysis/images/cyl_GL10S5_R500_2014-10-27_13-10-18}
    \caption{Fitting flat plates with spheres, not quite a good fit.}
    \label{fg:srdTable}
\end{figure}

Anyway, I hope this post may encourage some of you to start playing around mixing and matching morphologies of simulation and fit. 
It is an interesting topic that often comes up when discussing SAXS, and it will be worthwhile to test some more shapes (and it is definitely worthwhile to test your particular sample assumptions!).

A final note of warning: this is simulated data, unaffected by smearing and multiple scattering, and assuming sharp boundaries between the different phases. 
In real instruments you may actually change the slope of the scattering curve through (a combination of) any of these effects! 
Always support the shape assumption with data from other techniques, and do not rely on SAXS alone!

\subsection{Simulation details}\label{ssc:srdSim}

Q-range: 0.01 - 1 (1/nm), divided over 100 log-spaced points. Intensity normalized to 100000 virtual counts at q = 0.01. The uncertainty is estimated as the square root of the virtual counts or 1\% of the number of counts, whichever is largest.

[s1]: FF: Sphere, (number-weighted) R-distribution: Gaussian, sigma = 5 nm, X0 = 10 nm. Remaining parameters = 1.0. Vol-weighted distribution has a mode at ~15 nm, and a FWHM of about 10 nm. 64 datapoints have uncertainties limited to 1\% of the intensity.

[s2]: FF: Disc, (number-weighted) R-distribution: Gaussian, sigma = 5 nm, X0 = 10 nm. 75 datapoints have uncertainties limited to 1\% of the intensity.

[s3]: FF: Rod, (number-weighted) L-distribution: Gaussian, sigma = 5 nm, X0 = 10 nm. all datapoints have uncertainties limited to 1\% of the intensity.

[s4]: FF: Cylinder, (number-weighted) R-distribution: Gaussian, sigma = 5 nm, X0 = 10 nm. L = 500 nm.

[s5]: FF: Cylinder, (number-weighted) L-distribution: Gaussian, sigma = 5 nm, X0 = 10 nm. R = 500 nm.

\subsection{Fitting details}

Sphere model R-range 3.14 - 314 nm.

Disc model using "Isotropic Cylinders" with length set to 0.5 nm, and radius range 3.14 - 314 nm. Limited to 31.4 nm for faster convergence where appropriate.

Rod model using "Isotropic Cylinders" with radius set to 0.5 nm, and length range 3.14 - 314 nm. Limited to 31.4 nm for faster convergence where appropriate.

\subsection{Relevant Comments}
\subsubsection{from Dr. Martin Hollamby:}
\citet{Gawrys-2005} provides an interesting perspective on this subject (not my own work). 
It would also tend to disagree with the (in my opinion a little too simplistic) statement given in Yorijo Oba’s paper. 
To summarize, they noted that various different shapes had been used to fit SAS data arising from dispersions of asphaltenes, and analyzed several of their own data-sets using a variety of different models in order to judge the closest match using the goodness-of-fit parameter. 
In this case information from other techniques was not easily accessible (as is often the case in real systems). 
I guess the conclusion could therefore be (which I believe still fits with your analysis above) that choosing spheres over short cylinders to fit a single given data-set is potentially equally as misleading as choosing short cylinders over spheres if goodness-of-fit is not accounted for.

\section{Pitfalls}
\emph{SOURCE: \citet{Pauw-2013a}}

Whichever route is chosen for analysis, there are a few data analysis pitfalls to be wary of:
\begin{itemize}
\item{\textsc{Data linearisation}: } As noted by others, data linearisation has become unnecessary in current-day analyses \cite{Stribeck-2002}. 
They can be used as a quick evaluation of the data, but their use as a final recourse should be discouraged. In particular the actual graphical analysis of linearised data is a practice that should be retired. 
The most extreme cases, where the linearised data is not linear yet still subjected to graphical analyses, are occasionally encountered \cite{Das-2005,Wang-2008}. 
When linearisations are used to extract parameters, they should at least exhibit a linear region (as reiterated by \citet{Porod-1951}).
\item{\textsc{Uniqueness ``it fits, so it must be true''}: } As indicated in \S \ref{sc:ssa}, there are a large number of solutions that would fit a given scattering pattern, necessitating assumptions for two of the three morphological aspects of packing, polydispersity and shape. 
The assumptions should ideally be supported with supplementary techniques such as microscopy or porosimetry, or from fundamental considerations of the emergence of the scatterers (as expressly stated by \citet{Evrard-2005}).

In standard least-squares fitting procedures, however, assumptions have to be made on all three aspects which often leads to imperfect descriptions of the scattering data.
It may then transpire that, once a combination of aspects has been found that fits the data, it is thought that these must be the correct aspects as they describe the data (a form of circular reasoning similar to the logical fallacy of ``begging the question''). 
In other words, quoting Dr. J. Ilavsky: ``The fact that A model fits your data is NOT proof that it is THE appropriate model''. 
While this fallacy is not always clearly indicated, some papers do indicate that the success of a particular structural model to describe the data is evidence for its validity \cite{Tekobo-2011,Parent-2011,Meli-2012}. 
As mentioned, the choice of any particular model should be supported by supplementary information.

\item{\textsc{Lack of uncertainties and overly optimistic fits}: } 
Uncertainties allow for the weighting of the data to its uncertainty, so that accurate datapoints weigh more heavily in least-squares optimisation than datapoints with large uncertainties. 
Furthermore, the provision of uncertainties allows for determination of a goodness of fit value which indicates whether or not the model fits the data (on average) to within the uncertainty of the data \cite{Pedersen-1997,Pauw-2013}. 
A lack of uncertainties does not allow any further evaluation of a model than an estimation by eye, whose analysis capabilities are easily swayed by the choice of axes and datapoint size. 
Coupled with a modicum of ``wishful thinking'', this may lead to overly optimistic fits that only coarsely describe the data. Such fits still provide structural parameters, but their veracity is dubious. 
An example of optimistic data fitting is given by \cite{White-2010}, with poor fits to data with unknown uncertainties. 
Fortunately, their results appear to agree with TEM data. 

\item{\textsc{Unsuitable range}: } A scattering pattern is metrologically limited to a finite angular range with equally limited angular steps. 
It can be shown that the smallest measurable feature is closely related to the largest measurable angle through $R_\mathrm{min}\approx\pi/\max(q)$ \cite{Pauw-2013}. 
The largest measurable feature is ultimately limited to the angular divergence of the incident beam (as can be easily derived from considerations of the Bonse-Hart-type diffractometer). 
It can be approximated as $R_\mathrm{max}\approx\pi/w$, where $w$ is the width of the beam profile on the detector in $q$. 
It will in most cases be further limited by the smallest step size in $q$ of the detection system. 
When the determined structural features from data analysis contain information on sizes beyond these limits, a check for their evidence in the measured data must be made. 

\end{itemize}
Through recognition of these pitfalls as well as a thorough understanding of the limitations and applicability of the applied models, reliable data analysis may be achieved.

\section{Software}
